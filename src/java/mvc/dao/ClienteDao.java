/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.sql.DataSource;
import mvc.bean.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Deliane
 */
@Repository
public class ClienteDao {
  
    private final Connection connection;
    
    @Autowired
    public ClienteDao(DataSource dataSource){
        try {
            this.connection = dataSource.getConnection();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
    
    public boolean adicionaCliente(Cliente cliente){
        String sql = "insert into cliente"
                   + "(cli_nome, cli_email, cli_login, cli_senha) \n" +
                     "values(?,?,?,?);";
        
        try (PreparedStatement stmt = connection.prepareStatement(sql)){
            
            stmt.setString(1, cliente.getNome());
            stmt.setString(2, cliente.getEmail());
            stmt.setString(3, cliente.getLogin());
            stmt.setString(4, cliente.getSenha());
            stmt.execute();
            stmt.close();
            
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        
    return true;    
    } 
    
    public boolean editaCliente(Cliente cliente){
        String sql = "update cliente set "
                   + "cli_nome = ?, cli_email = ?, cli_login = ?, cli_senha = ?\n" +
                     "where cli_id = ?";
        try(PreparedStatement stmt = connection.prepareStatement(sql)) {
            
            stmt.setString(1, cliente.getNome());
            stmt.setString(2, cliente.getEmail());
            stmt.setString(3, cliente.getLogin());
            stmt.setString(4, cliente.getSenha());
            stmt.setLong(5, cliente.getId());
            
            
        } catch (Exception e) {
            throw  new RuntimeException(e);
        }
    return true;
    }
    
    

}
